﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Lab3.Services;
using Lab3.Views;

namespace Lab3
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
